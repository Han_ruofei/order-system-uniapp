import { baseUrl } from './config.js';
export function request(options, needToken = true) {
  return new Promise((resolve, reject) => {
    uni.request({
      url: baseUrl + options.url,
      method: options.method || 'GET',
      data: options.data || {},
      header: {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + uni.getStorageSync('sessionid')
      },
	  timeout:5000,
      success: res => {
		  // console.log(res)
        if (res.statusCode === 200) {
          resolve(res.data);
        } else {
          reject(res.errMsg);
        }
      },
      fail: err => {
        reject(err.errMsg);
      }
    });
  });
}