import App from './App'
import uView from 'uview-ui'
import store from 'store/index.js'
// #ifndef VUE3
import Vue from 'vue'
Vue.use(uView)

Vue.prototype.$store = store
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
	...App
})
Vue.prototype.baseUrl = 'http://127.0.0.1:8000'

// 这里需要写在最后，是为了等Vue创建对象完成，引入"app"对象(也即页面的"this"实例)

// http接口API集中管理引入部分
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
