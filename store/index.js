import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
	// 这个代表全局可以访问数据对象，就像是咱们在组件中声明的 data 属性
	state: {
		// 进行排序的菜品
		dishWithType: [],
		// 原始的菜品列表
		dishList:[],
		// 总价
		totalPrice: 0,
		// 菜品总数量
		totalNum: 0,
		loginState: false,
		// orderFlavor:[],
		userInfo: {
			name: '',
			avatar_url: '',
			integral: 0,
			sessionid: '',
		},
		// 当前订单
		// dishCart:[]
	},
	// 这个实时监听 state 内的数据对象变化，类似 咱们组件中的 computed 属性，会依赖 state 数据变化而变化
	getters: {
		// 获取大于1的food
		curCart(state){
			const res = []
			state.dishWithType.forEach(item=>{
				item.forEach(dish=>{
					if(dish.num>0){
						res.push(dish)
					}
				})
			})
			// console.log('当前vuex中的购物车中的商品',res)
			return res
		}
	},
	// 用来同步设置 state 的值
	mutations: {
		// 登入
		LOGIN(state, user_info) {
			console.log('vuex接收到的数据', user_info)
			state.loginState = true
			state.userInfo.name = user_info.name
			state.userInfo.avatar_url = user_info.avatar
			state.userInfo.sessionid = user_info.sessionid
			state.userInfo.integral = user_info.integral
			uni.setStorageSync('sessionid', user_info.sessionid)

			// state.sessionid = sessionid
			uni.setStorageSync('sessionid', user_info.sessionid)
			uni.setStorageSync('userInfo', JSON.stringify(user_info))
		},
		// 登出
		LOGOUT(state) {
			state.loginState = false
			state.userInfo.name = ""
			state.userInfo.integral = 0
			state.userInfo.avatar = ""
			state.userInfo.sessionid = ""
			uni.removeStorageSync('sessionid')
			uni.removeStorageSync('userInfo')
			// state.dishList.forEach(item=>{
			// 	item.num=0
			// })
			state.dishWithType.forEach(item=>{
				item.forEach(dish=>{
					dish.num=0
				})
			})
		},
		// 存储菜品
		ADD2CART(state, {dish, add} ) {
			let food = null
			// 遍历二维数组找到与之对应的dish
			state.dishWithType.forEach(item => {
				item.forEach(dishs => {
					if (dishs.id === dish.id) {
						food = dishs
					}
				})
			})
			if (add) {
				food.num++
				state.totalNum++
				state.totalPrice += dish.price
			}else{
				food.num--
				state.totalNum--
				state.totalPrice -= dish.price
			}
		},
		// 整理数据，将数据添加道dishWithType中
		ADD2DISH(state, dishes) {
			state.dishWithType.push(dishes)
		},
		// 添加原始数据
		ADDDISHLIST(state,list){
			// console.log('当前的dishList添加成功',list)
			state.dishList= [...list]
		},
		// 设置totalNum以及totalPrice
		clearCart(state){
			state.totalNum = 0
			state.totalPrice = 0
			state.dishList.forEach(item=>{
				item.num=0
			})
		}
		// // 存储订单口味
		// ADD2FLAVOR(state,flavorList){
		// 	state.orderFlavor = flavorList
		// }

	},
	// 通过提交 mutations 内部的方法，异步更新 state 的状态，官方推荐都使用这种方法比较合适
	actions: {
	
	}
})
export default store
